import { AppDataSource } from "./data-source";
import { Type } from "./entity/Type";

AppDataSource.initialize()
  .then(async () => {
    const typeRepository = AppDataSource.getRepository(Type);
    await typeRepository.clear();
    console.log("Inserting a new type into the database...");
    var type = new Type();
    type.id = 1;
    type.name = "drink";
    console.log("Inserting a new type into the database...");
    await typeRepository.save(type);

    var type = new Type();
    type.id = 2;
    type.name = "bekery";
    console.log("Inserting a new type into the database...");
    await typeRepository.save(type);

    var type = new Type();
    type.id = 3;
    type.name = "food";
    console.log("Inserting a new type into the database...");
    await typeRepository.save(type);

    const types = await typeRepository.find({ order: { id: "asc" } });
    console.log(types);
  })
  .catch((error) => console.log(error));
