import { AppDataSource } from "./data-source";
import { Role } from "./entity/Role";
import { User } from "./entity/User";

AppDataSource.initialize()
  .then(async () => {
    const roleRepository = AppDataSource.getRepository(Role);
    const userRepository = AppDataSource.getRepository(User);

    const adminRole = await roleRepository.findOneBy({ id: 1 });
    const userRole = await roleRepository.findOneBy({ id: 2 });
    console.log("Inserting a new user into the database...");
    var user = new User();
    user.id = 1;
    user.email = "admin@mail.com";
    user.password = "123456";
    user.gender = "male";
    user.roles = [];
    user.roles.push(adminRole);
    user.roles.push(userRole);
    console.log("Inserting a new user into the database...");
    await userRepository.save(user);

    // const admin = await userRepository.findOneBy({ id: 1 });
    // console.log(admin);

    var user = new User();
    user.id = 2;
    user.email = "user1@mail.com";
    user.password = "123456";
    user.gender = "male";
    user.roles = [];
    user.roles.push(userRole);
    console.log("Inserting a new user into the database...");
    await userRepository.save(user);

    // const user1 = await userRepository.find({
    //   where: { id: 2, gender: "male" },
    // });
    // console.log(user1[0]);

    var user = new User();
    user.id = 3;
    user.email = "user2@mail.com";
    user.password = "123456";
    user.gender = "female";
    user.roles = [];
    user.roles.push(userRole);
    console.log("Inserting a new user into the database...");
    await userRepository.save(user);

    //ใช้ [] เพื่อเป็น OR ใช้ {} คือ AND
    // const user2 = await userRepository.findOne({
    //   where: [{ id: 3 }, { gender: "female" }],
    // });

    const users = await userRepository.find({
      relations: { roles: true },
    });

    console.log(JSON.stringify(users, null, 2));

    const roles = await roleRepository.find({
      relations: { users: true },
    });

    console.log(JSON.stringify(roles, null, 2));
  })
  .catch((error) => console.log(error));
