import { AppDataSource } from "./data-source";
import { Role } from "./entity/Role";
import { User } from "./entity/User";

AppDataSource.initialize()
  .then(async () => {
    const userRepository = AppDataSource.getRepository(User);
    const roleRepository = AppDataSource.getRepository(Role);
    await userRepository.clear();
    await roleRepository.clear();
    console.log("Inserting a new role into the database...");
    var role = new Role();
    role.id = 1;
    role.name = "admin";
    console.log("Inserting a new role into the database...");
    await roleRepository.save(role);

    var role = new Role();
    role.id = 2;
    role.name = "user";
    console.log("Inserting a new role into the database...");
    await roleRepository.save(role);

    const roles = await roleRepository.find({ order: { id: "asc" } });
    console.log(roles);
  })
  .catch((error) => console.log(error));
